<?php

namespace SayHello\Theme\Widget;

use WP_Widget_Recent_Posts;
use WP_Query;

/**
 * Extend Recent Posts Widget
 *
 * Adds different formatting to the default WordPress Recent Posts Widget
 */

class RecentPosts extends WP_Widget_Recent_Posts
{

	public function run()
	{
		add_action('widgets_init', function () {
			unregister_widget('\\WP_Widget_Recent_Posts');
			register_widget('\SayHello\Theme\Widget\RecentPosts');
		});
	}

	public function widget($args, $instance)
	{

		if (! isset($args['widget_id'])) {
			$args['widget_id'] = $this->id;
		}

		$title = ( ! empty($instance['title']) ) ? $instance['title'] : __('Recent Posts');

		/** This filter is documented in wp-includes/widgets/class-wp-widget-pages.php */
		$title = apply_filters('widget_title', $title, $instance, $this->id_base);

		$number = ( ! empty($instance['number']) ) ? absint($instance['number']) : 5;
		if (! $number) {
			$number = 5;
		}
		$show_date = isset($instance['show_date']) ? $instance['show_date'] : false;

		/**
		 * Filters the arguments for the Recent Posts widget.
		 *
		 * @since 3.4.0
		 * @since 4.9.0 Added the `$instance` parameter.
		 *
		 * @see WP_Query::get_posts()
		 *
		 * @param array $args     An array of arguments used to retrieve the recent posts.
		 * @param array $instance Array of settings for the current widget.
		 */
		$r = new WP_Query(
			apply_filters(
				'widget_posts_args',
				array(
					'posts_per_page'      => $number,
					'no_found_rows'       => true,
					'post_status'         => 'publish',
					'ignore_sticky_posts' => true,
				),
				$instance
			)
		);

		if (! $r->have_posts()) {
			return;
		}
		?>
		<?php echo $args['before_widget']; ?>
		<?php
		if ($title) {
			echo $args['before_title'] . $title . $args['after_title'];
		}
		?>
		<ul>
			<?php foreach ($r->posts as $recent_post) :
				$post_title = get_the_title($recent_post->ID);
				$title      = ( ! empty($post_title) ) ? $post_title : __('(no title)');
				?>
				<li>
					<h2 class="widget__posttitle"><a href="<?php the_permalink($recent_post->ID); ?>"><?php echo $title; ?></a></h2>
					<div class="widget__postexcerpt">
						<?php if ($show_date) : ?>
							<span class="widget__postdate"><?php echo get_the_date('', $recent_post->ID); ?></span>
						<?php endif; ?>
						<?php //echo get_the_excerpt($recent_post->ID); ?>
					</div>
					<a class="widget__thumbnaillink" href="<?php the_permalink($recent_post->ID); ?>"><?php
					if (has_post_thumbnail($recent_post->ID)) {
						echo get_the_post_thumbnail($recent_post->ID, 'thumbnail');
					} else {
						echo '<div class="widget__thumbnailplaceholder"></div>';
					}
					?></a>
				</li>
			<?php endforeach; ?>
		</ul>
		<?php
		echo $args['after_widget'];
	}
}
