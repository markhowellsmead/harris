<?php

namespace SayHello\Theme\Components;

use Timber\PostQuery;

class PostsByTagDataProvider
{
	public function getData($component)
	{
		if (empty($component['posts'])) {
			$component['posts'] = new PostQuery([
				'ignore_sticky_posts' => true,
				'posts_per_page' => $component['number'],
				'tag_id' => $component['tag']
			]);
		}

		return $component;
	}
}
