<?php

namespace SayHello\Theme\Components;

use Timber\Timber;

class CollectionsDataProvider
{
	public function getData($component)
	{
		foreach ($component['collections'] as &$collection) {
			$collection->posts = Timber::get_posts([
				'posts_per_page' => -1,
				'tax_query' => [
					[
						'taxonomy' => $collection->taxonomy,
						'field' => 'term_id',
						'terms' => $collection->term_id
					]
				]
			]);
			$collection->link = get_term_link($collection->term_id);
		}
		return $component;
	}
}
