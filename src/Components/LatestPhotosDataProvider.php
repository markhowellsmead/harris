<?php

namespace SayHello\Theme\Components;

use Timber\PostQuery;

class LatestPhotosDataProvider
{
	public function getData($component)
	{

		if (empty($component['posts'])) {
			$component['posts'] = new PostQuery([
				'post_type' => 'photo',
				'posts_per_page' => $component['number'],
				'ignore_sticky_posts' => true
			]);
		}

		// Fix array length if there is a sticky post
		// if (count($component['posts']) > $component['number']) {
		// 	$component['posts'] = array_slice($component['posts'], 0, $component['number']);
		// }

		return $component;
	}
}
