<?php

namespace SayHello\Theme\Packages;

use SayHello\Core\Base\Contracts\Package;
use Timber\Timber;
use Timber\Image as TimberImage;
use Timber\ImageHelper as ImageHelper;
use Timber\Menu as TimberMenu;
use Twig_SimpleFilter;
use Twig\TwigFilter;

/**
 * Groovy stuff for Twig
 *
 * @author Mark Howells-Mead <mark@sayhello.ch>
 * @version 1.0
 */
class Twig
{
	public function run()
	{
		add_action('timber/twig/filters', [$this, 'addFilters'], 10, 1);
		add_filter('timber_context', [$this, 'addData'], 10, 1);
		add_filter('timber/post/content/show_password_form_for_protected', [$this, 'showPasswordFormForProtectedPosts'], 10, 0);
		add_filter('timber_context', [$this, 'addNavigationsGlobally'], 10, 1);
	}

	/**
	 * Adds all navigations defined in /congig/navigations.php to the global Timber context.
	 *
	 * @see Timber\Timber
	 * @param array $context existing Timber context
	 */
	public function addNavigationsGlobally($context)
	{
		$menus = get_registered_nav_menus();
		foreach (array_keys($menus) as $key) {
			$context[$key] = new TimberMenu($key);
		}
		return $context;
	}

	/**
	 * Add functions to Twig
	 * @param \Twig_Environment $twig The Twig environment object
	 */
	public function addFilters($twig)
	{

		/**
		 * Resize image to fit within width and height constraints
		 * https://github.com/timber/timber/issues/1332
		 * Usage: {{ image.src('original')|fit(600, 400) }}
		 */
		$twig->addFilter(new TwigFilter('fit', function ($src, $w, $h = 0) {
			// Instantiate TimberImage from $src so we have access to dimensions
			$img = new TimberImage($src);

			// If the image is smaller on both width and height, return original
			if ($img->width() <= $w && $img->height() <= $h) {
				return $src;
			}

			// Compute aspect ratio of target box
			$aspect = $w / $h;

			// Call proportional resize on width or height, depending on how the image's
			// aspect ratio compares to the target box aspect ratio
			if ($img->aspect() > $aspect) {
				return ImageHelper::resize($src, $w);
			} else {
				return ImageHelper::resize($src, 0, $h);
			}
		}));

		$twig->addFilter(new TwigFilter('video_thumbnail', function ($url) {
			return sht_theme()->Packages->Media::getVideoThumbnail($url);
		}));

		return $twig;
	}

	/**
	 * Add data to Twig
	 *
	 * @return void
	 */
	public function addData($context)
	{
		if (function_exists('pll_the_languages')) {
			$context['languages'] = pll_the_languages([
				'echo' => 0,
				'hide_if_no_translation' => 1,
				'raw' => 1
			]);
		}
		if (function_exists('pll_current_language')) {
			$context['current_language'] = pll_current_language();
		}

		$context['queried_object'] = get_queried_object();

		$context['page_for_posts'] = Timber::get_post(get_option('page_for_posts'));
		$context['photo_archive_link'] = get_post_type_archive_link('photo');

		return $context;
	}

	/**
	 * We need to manually define whether password-protected posts should actually be protected.
	 * To customize the form itself, use the 'timber/post/content/password_form' filter in your Theme.
	 * @return boolean       Yes please
	 */
	public function showPasswordFormForProtectedPosts()
	{
		return true;
	}
}
