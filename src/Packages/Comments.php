<?php

namespace SayHello\Theme\Packages;

use Timber\Timber;
use WP_Comment;

/**
 * Stuff for regular WordPress commenting function
 *
 * @author Mark Howells-Mead <mark@sayhello.ch>
 * @version 1.0
 */
class Comments
{
	public function run()
	{
		add_action('wp_enqueue_scripts', [$this, 'maybeEnqueueReplyScript']);
	}

	public function maybeEnqueueReplyScript()
	{
		if (is_singular() && comments_open() && get_option('thread_comments')) {
			wp_enqueue_script('comment-reply');
		}
	}

	/**
	 * Custom callback function to render a single post comment
	 * @param  \WP_Comment $comment The comment object
	 * @param  array $args    The array of list options
	 * @param  int $depth   The depth to which the list descends
	 * @return void
	 */
	public static function formatComment(WP_Comment $comment, array $args, int $depth)
	{
		Timber::render(['partials/comments-single-' .get_post_type(). '.twig', 'partials/comments-single.twig'], [
			'comment' => $comment,
			'args' => $args,
			'depth' => $depth
		]);
	}
}
