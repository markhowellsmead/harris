<?php

namespace SayHello\Theme\Packages;

use Timber\Timber;

/**
 * Sidebar stuff
 *
 * @author Mark Howells-Mead <mark@sayhello.ch>
 */
class Sidebars
{

	private $sidebars;

	public function __construct()
	{
		// Fixed sidebars, hard-coded into the specific layout locations
		$this->sidebars = [
			[
				'name' => __('Blog list', 'harris'),
				'id' => 'bloglist',
				'description' => __('Widgets in this area will appear to the right of the list of blog posts.', 'harris'),
				'before_widget' => '<div class="c-widget %2$s">',
				'after_widget' => '</div>',
				'before_title' => '<p class="c-widget__title">',
				'after_title' => '</p>',
			],
			[
				'name' => __('Blog single', 'harris'),
				'id' => 'blogsingle',
				'description' => __('Widgets in this area will appear on a single blog post.', 'harris'),
				'before_widget' => '<div class="c-widget %2$s">',
				'after_widget' => '</div>',
				'before_title' => '<p class="c-widget__title">',
				'after_title' => '</p>',
			],
			[
				'name'          => __('Main sidebar', 'harris'),
				'id'            => 'sidebar_main',
				'description'   => __('Widget area on the right of the page', 'harris'),
				'before_widget' => '<div class="widget %2$s">',
				'after_widget'  => '</div>',
				'before_title'  => '<p class="widget-title">',
				'after_title'   => '</p>',
			],
			[
				'name'          => __('Footer left', 'harris'),
				'id'            => 'footer1',
				'description'   => __('Widget area in the footer of the page', 'harris'),
				'before_widget' => '<div class="widget %2$s">',
				'after_widget'  => '</div>',
				'before_title'  => '<p class="widget-title">',
				'after_title'   => '</p>',
			],
			[
				'name'          => __('Footer middle', 'harris'),
				'id'            => 'footer2',
				'description'   => __('Widget area in the footer of the page', 'harris'),
				'before_widget' => '<div class="widget %2$s">',
				'after_widget'  => '</div>',
				'before_title'  => '<p class="widget-title">',
				'after_title'   => '</p>',
			],
			[
				'name'          => __('Footer right', 'harris'),
				'id'            => 'footer3',
				'description'   => __('Widget area in the footer of the page', 'harris'),
				'before_widget' => '<div class="widget %2$s">',
				'after_widget'  => '</div>',
				'before_title'  => '<p class="widget-title">',
				'after_title'   => '</p>',
			],
		];
	}

	/**
	 * Add hooks for this Package type
	 * @return void
	 */
	public function run()
	{
		if (count($this->sidebars)) {
			add_action('after_setup_theme', [$this, 'themeSupport']);
			add_action('widgets_init', [$this, 'register']);
			add_filter('timber_context', [$this, 'extendTimberContext']);
		}
	}

	/**
	 * Add Theme support for Widget Sidebar areas
	 * @return void
	 */
	public function themeSupport()
	{
		add_theme_support('sidebars');
	}

	/**
	 * Adds all sidebars to the global Timber context.
	 *
	 * @see Timber\Timber
	 * @param array $context existing Timber context
	 */
	public function extendTimberContext($context)
	{
		if (!empty($this->sidebars)) {
			if (!isset($context['sidebars'])) {
				$context['sidebars'] = [];
			}
			foreach (array_values($this->sidebars) as $data) {
				$context['sidebars'][$data['id']] = Timber::get_widgets($data['id']);
			}
		}

		return $context;
	}

	/**
	 * Register the sidebars detailed in the two sidebars arrays
	 * @return void
	 */
	public function register()
	{
		foreach ($this->sidebars as $sidebar) {
			register_sidebar($sidebar);
		}
	}
}
