<?php

namespace SayHello\Theme\Packages;

use Timber\Timber;

/**
 * Stuff for media
 *
 * @author Mark Howells-Mead <mark@sayhello.ch>
 * @version 1.0
 */
class Media
{
	public function run()
	{
		add_filter('oembed_result', [$this, 'cleanEmbedParameters'], 10, 2);
		// add_filter('oembed_result', [$this, 'wrapVideoHTML'], 20, 2);
		add_filter('image_resize_dimensions', [$this, 'allowUpscale'], 10, 6);
	}

	public static function getVideoThumbnailTag($source_url)
	{
		$src = self::getVideoThumbnail($source_url);

		if (empty($src)) {
			return '';
		}

		return '<img src="' .$src. '" alt="Video preview image" class="post-thumbnail-image video-ref-image" data-videoURL="' .$source_url. '">';
	}


	/**
	 * Get remote video thumbnail URL
	 *
	 * @param string $source_url The video URL
	 * @return string The Video Thumbnail URL
	 **/
	public static function getVideoThumbnail($source_url)
	{
		if ($source_url == '' || is_array($source_url)) {
			return '';
		}

		// angabe ohne url gibt leeres string zurück
		$atts = [
			'url' => $source_url
		];

		if (!is_string($atts['url'])) {
			return '';
		}

		$aPath = parse_url($atts['url']);
		$aPath['host'] = str_replace('www.', '', $aPath['host']);

		switch ($aPath['host']) {
			case 'youtu.be':
				$atts['id'] = preg_replace('~^/~', '', $aPath['path']);
				return '//i.ytimg.com/vi/'.$atts['id'].'/maxresdefault.jpg';
			break;

			case 'youtube.com':
				$aParams = explode('&', $aPath['query']);
				foreach ($aParams as $param) :
					// nach parameter 'v' suchen
					$thisPair = explode('=', $param);
					if (strtolower($thisPair[0]) == 'v') :
						$atts['id'] = $thisPair[1];
						break;
					endif;
				endforeach;
				if (!isset($atts['id']) || !$atts['id']) {
					return '';    // wenn ID nicht verfügbar, gibt leeres string zurück
				} else {
					return '//i.ytimg.com/vi/'.$atts['id'].'/maxresdefault.jpg'; // gibt 1. thumbnail-bild-src zurück.
				}
				break;

			case 'vimeo.com':
				$urlParts = explode('/', $atts['url']);
				$hash = @unserialize(@file_get_contents('https://vimeo.com/api/v2/video/'.$urlParts[3].'.php'));
				if ($hash && $hash[0] && (isset($hash[0]['thumbnail_large']) && $hash[0]['thumbnail_large'] !== '')) {
					return $hash[0]['thumbnail_large'];
				} else {
					return '';//$this->template_uri.'/img/listicon_video.jpg';
				}
				break;

			default:
				// gibt leeres string zurück
				return '';
			break;
		}
	}

	public function cleanEmbedParameters($html, $url)
	{
		$host = parse_url($url, PHP_URL_HOST);
		switch ($host) {
			case 'youtube.com':
			case 'www.youtube.com':
			case 'youtu.be':
				$html = str_replace('?feature=oembed', '?feature=oembed&hl=en&amp;fs=1&amp;showinfo=0&amp;rel=0&amp;iv_load_policy=3&amp;hd=1&amp;vq=hd720&amp;version=3&amp;autohide=1&amp;wmode=opaque&amp;cc=1', $html);
				break;
		}

		return $html;
	}

	public function wrapVideoHTML($html, $url = '')
	{
		preg_match('~[www\.]?(vimeo\.|wordpress\.tv|youtube|youtu\.be)~', $url, $matches);

		if (!is_feed() && (!empty($matches))) {
			$html = sprintf(
				'<div class="responsive-embed widescreen %1$s">%2$s</div>',
				sanitize_title_with_dashes($matches[0]),
				$html
			);
		}

		return $html;
	}

	public function allowUpscale($default, $orig_w, $orig_h, $new_w, $new_h, $crop)
	{
		if (!$crop) {
			return null;
		} // let the wordpress default function handle this

		$size_ratio = max($new_w / $orig_w, $new_h / $orig_h);

		$crop_w = round($new_w / $size_ratio);
		$crop_h = round($new_h / $size_ratio);

		$s_x = floor(($orig_w - $crop_w) / 2);
		$s_y = floor(($orig_h - $crop_h) / 2);

		return array( 0, 0, (int) $s_x, (int) $s_y, (int) $new_w, (int) $new_h, (int) $crop_w, (int) $crop_h );
	}

	public function rewritePostFormatBase($slug)
	{
		return 'type';
	}
}
