<?php

namespace SayHello\Theme\Packages;

class Post
{
	public function run()
	{
		add_filter('post_class', [$this, 'postClasses'], 10, 3);
	}

	public function postClasses($classes, $class, $post_id)
	{
		if (!has_post_thumbnail($post_id)) {
			$classes[] = 'with--nothumbnail';
		}
		return $classes;
	}
}
