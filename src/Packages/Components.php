<?php

namespace SayHello\Theme\Packages;

/**
 * Stuff for all Components
 *
 * @author Mark Howells-Mead <mark@sayhello.ch>
 * @version 1.0
 */
class Components
{

	/**
	 * If there is a DataProvider class in the Theme (src/Components/) for the current builder
	 * component then run the getData function and apply additional data to the component
	 * @param  array $component the initial version of the component (pass by reference)
	 * @return void
	 */
	public static function extendComponent($component)
	{
		if (isset($component['acf_fc_layout'])) {
			$componentName = $component['acf_fc_layout'];
			$className = self::buildClassName($componentName);
			if (class_exists($className)) {
				$dataProvider = new $className();
				$component = $dataProvider->getData($component);
			}
		}
		return $component;
	}

	private static function buildClassName($name)
	{
		//Replace _ with a space, uppercase words, remove spaces
		$baseClassName = str_replace(' ', '', ucwords(str_replace('_', ' ', str_replace('-', ' ', $name))));
		return "SayHello\Theme\Components\\{$baseClassName}DataProvider";
	}
}
