<?php

namespace SayHello\Theme\Packages;

use Timber\Timber;
use WP_Comment;

/**
 * Stuff for Gutenberg
 *
 * @author Mark Howells-Mead <mark@sayhello.ch>
 * @version 1.0
 */
class Gutenberg
{

	public $theme = [];

	public function __construct()
	{
		$this->theme = wp_get_theme();
	}

	public function run()
	{
		add_action('enqueue_block_editor_assets', [ $this, 'registerBackendAssets' ]);
	}

	public function registerBackendAssets()
	{
		wp_enqueue_script(
			'harris-blocks',
			get_template_directory_uri() . '/assets/dist/scripts/blocks/blocks.min.js',
			['wp-blocks'],
			$this->theme->Version,
			true
		);
	}
}
