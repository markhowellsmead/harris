<?php

namespace SayHello\Theme\Packages;

use enshrined\svgSanitize\Sanitizer;

/**
 * Implements SVG Support to Wordpress, cleaning SVG Files before they are saved.
 *
 * @author Mark Howells-Mead <mark@sayhello.ch>
 * @version 1.1
 */
class SVGSupport
{

	/**
	 * Hooks the upload_mimes and wp_handle_upload_prefilter filter.
	 * Hooks the admin_enqueue_scripts action.
	 *
	 * @return void
	 */
	public function run()
	{
		add_filter('upload_mimes', [$this, 'allowSvgUpload']);
		add_filter('wp_handle_upload_prefilter', [$this, 'sanitizeSvg']);
		add_action('admin_enqueue_scripts', [$this, 'addSvgStyles']);
	}

	/**
	 * Adds inline Style to properly display SVG files.
	 *
	 * @return void
	 */
	public function addSvgStyles()
	{
		wp_add_inline_style('wp-admin', "img.attachment-80x60[src$='.svg'] { width: 100%; height: auto; }");
	}

	/**
	 * Adds the 'image/svg+xml' mime type to the list of allowed upload filetypes.
	 *
	 * @param  Array $mimeTypes  Allowed mime types.
	 * @return Array             Allowed mime types with SVGs.
	 */
	public function allowSvgUpload($mimeTypes)
	{
		$mimeTypes['svg'] = 'image/svg+xml';

		return $mimeTypes;
	}

	/**
	 * Sanitizes a SVG file before it's saved to the server storage.
	 * This removes unallowed tags and scripts.
	 *
	 * @see enshrined\svgSanitize\Sanitizer
	 * @param  Array $file  Uploaded file.
	 * @return Array        Cleaned file if type is SVG.
	 */
	public function sanitizeSvg($file)
	{
		if ($file['type'] == 'image/svg+xml') {
			$sanitizer = new Sanitizer();
			$dirtySVG = file_get_contents($file['tmp_name']);
			$sanitizedSvg = $sanitizer->sanitize($dirtySVG);

			global $wp_filesystem;
			$credentials = request_filesystem_credentials(site_url() . '/wp-admin/', '', false, false, array());
			if (!WP_Filesystem($credentials)) {
				request_filesystem_credentials(site_url() . '/wp-admin/', '', true, false, null);
			}

			// Using the filesystem API provided by WordPress, we replace the contents of the temporary file and then let the process continue as normal.
			$wp_filesystem->put_contents($file['tmp_name'], $sanitizedSvg, FS_CHMOD_FILE);
		}

		return $file;
	}
}
