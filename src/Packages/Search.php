<?php

namespace SayHello\Theme\Packages;

use Timber\Timber;

/**
 * Stuff for WordPress search
 *
 * @author Mark Howells-Mead <mark@sayhello.ch>
 * @version 1.0
 */
class Search
{
	public function run()
	{
		add_filter('get_search_form', [$this, 'customForm'], 10, 0);
	}

	/**
	 * Generate custom search form
	 *
	 * @param string $form Form HTML.
	 * @return string Modified form HTML.
	 **/
	public function customForm()
	{
		return Timber::fetch('partials/search-form.twig');
	}
}
