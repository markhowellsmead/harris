<?php

namespace SayHello\Theme\Packages;

/**
 * Stuff for Post Formats
 *
 * @author Mark Howells-Mead <mark@sayhello.ch>
 * @version 1.0
 */
class PostFormat
{
	public function run()
	{

		// From http://justintadlock.com/archives/2012/09/11/custom-post-format-urls

		/* Remove core WordPress filter. */
		remove_filter('term_link', '_post_format_link', 10);

		/* Add custom filter. */
		add_filter('term_link', [$this, 'postFormatLink'], 10, 3);

		/* Remove the core WordPress filter. */
		remove_filter('request', '_post_format_request');

		/* Add custom filter. */
		add_filter('request', [$this,'postFormatRequest']);

		add_filter('single_term_title', [$this, 'singleTermTitle']);
	}

	public function postFormatSlugs()
	{

		$slugs = [
			'aside'   => 'aside',
			'audio'   => 'audio',
			'chat'    => 'chat',
			'gallery' => 'gallery',
			'image'   => 'photo',
			'link'    => 'link',
			'quote'   => 'quote',
			'status'  => 'status-update',
			'video'   => 'video'
		];

		return $slugs;
	}

	/**
	 * Filters post format links to use a custom slug.
	 *
	 * @param string $link The permalink to the post format archive.
	 * @param object $term The term object.
	 * @param string $taxnomy The taxonomy name.
	 * @return string
	 */
	public function postFormatLink($link, $term, $taxonomy)
	{
		global $wp_rewrite;

		if ('post_format' != $taxonomy) {
			return $link;
		}

		$slugs = $this->postFormatSlugs();

		$slug = str_replace('post-format-', '', $term->slug);
		$slug = isset($slugs[ $slug ]) ? $slugs[ $slug ] : $slug;

		if ($wp_rewrite->get_extra_permastruct($taxonomy)) {
			$link = str_replace("/{$term->slug}", '/' . $slug, $link);
		} else {
			$link = add_query_arg('post_format', $slug, remove_query_arg('post_format', $link));
		}

		return $link;
	}

	/**
	 * Changes the queried post format slug to the slug saved in the database.
	 *
	 * @param array $qvs The queried variables.
	 * @return array
	 */
	public function postFormatRequest($qvs)
	{

		if (!isset($qvs['post_format'])) {
			return $qvs;
		}

		$slugs = $this->postFormatSlugs();

		if ($key = array_search($qvs['post_format'], $slugs)) {
			$qvs['post_format'] = 'post-format-' . $key;
		}

		$tax = get_taxonomy('post_format');

		if (!is_admin()) {
			$qvs['post_type'] = $tax->object_type;
		}

		return $qvs;
	}

	public function singleTermTitle($name)
	{
		if (strtolower($name) === 'image') {
			$name = _x('Photos', 'Single term title', 'harris');
		}

		return $name;
	}
}
