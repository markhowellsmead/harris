<?php

namespace SayHello\Theme;

use Timber\Timber;
use Timber\Image as TimberImage;

/**
 * Theme class which gets loaded in functions.php.
 * Defines the Starting point of the Theme and registers Packages.
 *
 * Must be renamed to a suitable name for your current project (e.g. wptheme.<projectname> => class <Projectname>Theme).
 *
 * @author Mark Howells-Mead <mark@sayhello.ch>
 * @version 1.0
 */
class Theme
{
	private static $instance;
	private $theme;
	private $sidebars;
	private $translations;
	private $name;
	private $version;
	private $prefix;

	/**
	 * Creates an instance if one isn't already available,
	 * then return the current instance.
	 *
	 * @return object       The class instance.
	 */
	public static function getInstance()
	{
		if (! isset(self::$instance) && ! (self::$instance instanceof Theme)) {
			self::$instance = new Theme;

			self::$instance->name    = self::$instance->theme->name;
			self::$instance->version = self::$instance->theme->version;
			self::$instance->prefix  = 'sht';
		}

		return self::$instance;
	}

	public function __construct()
	{
		$this->theme = wp_get_theme();

		$this->translations = [
			_x('Search', 'Search form button', 'harris'),
		];
	}

	public function run()
	{

		$this->loadClasses(
			[
				\SayHello\Theme\Packages\Comments::class,
				\SayHello\Theme\Packages\Components::class,
				\SayHello\Theme\Packages\Gutenberg::class,
				\SayHello\Theme\Packages\LoginScreen::class,
				\SayHello\Theme\Packages\Media::class,
				\SayHello\Theme\Packages\Post::class,
				\SayHello\Theme\Packages\PostFormat::class,
				\SayHello\Theme\Packages\Search::class,
				\SayHello\Theme\Packages\ThemeMods::class,
				\SayHello\Theme\Packages\Sidebars::class,
				\SayHello\Theme\Packages\SVGSupport::class,
				\SayHello\Theme\Packages\Twig::class,
				\SayHello\Theme\Components\CollectionsDataProvider::class,
				\SayHello\Theme\Components\Hero::class,
				\SayHello\Theme\Block\ImageGallery::class,
				\SayHello\Theme\Widget\RecentPosts::class,
				\SayHello\Theme\Widget\SayHelloSponsor::class,
			]
		);

		add_action('wp_enqueue_scripts', [ $this, 'registerStyles' ]);
		add_action('wp_enqueue_scripts', [ $this, 'registerScripts' ]);

		add_action('admin_init', [ $this, 'editorStyles' ]);

		add_action('init', [ $this, 'addNavigations' ]);
		add_action('after_setup_theme', [ $this, 'loadTranslations' ]);
		add_action('after_setup_theme', [ $this, 'addThemeSupports' ]);
		add_action('after_setup_theme', [ $this, 'addImageSizes' ]);
		add_action('after_setup_theme', [ $this, 'contentWidth' ]);
		add_filter('image_size_names_choose', [$this, 'selectableImageSizes']);

		add_filter('body_class', [ $this, 'customizeBodyClass' ], 10, 1);
		add_action('wp_head', [ $this, 'customToolbarTextColour' ], 10);
		add_filter('post_class', [ $this, 'postClasses' ], 10, 1);
	}

	/**
	 * Loads the provided classes.
	 *
	 * @param $classes
	 */
	private function loadClasses($classes)
	{

		foreach ($classes as $class) {
			$class_parts = explode('\\', $class);
			$class_short = end($class_parts);
			$class_set   = $class_parts[ count($class_parts) - 2 ];

			if (! isset(sht_theme()->{$class_set}) || ! is_object(sht_theme()->{$class_set})) {
				sht_theme()->{$class_set} = new \stdClass();
			}

			if (property_exists(sht_theme()->{$class_set}, $class_short)) {
				wp_die(sprintf(__('A problem has ocurred in the Theme. Only one PHP class named “%1$s” may be assigned to the “%2$s” object in the Theme.', 'sht'), $class_short, $class_set), 500);
			}

			sht_theme()->{$class_set}->{$class_short} = new $class();

			if (method_exists(sht_theme()->{$class_set}->{$class_short}, 'run')) {
				sht_theme()->{$class_set}->{$class_short}->run();
			}
		}


		foreach ($classes as $class) {
			$instance = new $class();
			if (method_exists($instance, 'run')) {
				$instance->run();
			}
		}
	}

	/**
	 * Allow the Theme to use additional core features
	 */
	public function addThemeSupports()
	{
		add_theme_support('automatic-feed-links');
		add_theme_support('custom-logo');
		add_theme_support('html5', [ 'comment-list', 'comment-form', 'search-form', 'gallery', 'caption' ]);
		add_theme_support('menu');
		add_theme_support('post-thumbnails', [ 'post', 'page', 'photo' ]);
		add_theme_support('title-tag');
		add_theme_support('post-formats', [ 'image', 'gallery', 'video' ]);
		add_theme_support('sidebars');
		add_theme_support('align-wide');
		add_theme_support('disable-custom-colors');
		add_theme_support(
			'editor-color-palette',
			[
				[
					'name'  => esc_html__('Black', 'harris'),
					'slug' => 'black',
					'color' => '#000',
				],
				[
					'name'  => esc_html__('Light gray', 'harris'),
					'slug' => 'light-gray',
					'color' => '#777',
				],
				[
					'name'  => esc_html__('Yellow', 'harris'),
					'slug' => 'yellow',
					'color' => '#ffc',
				],
				[
					'name'  => esc_html__('Blue', 'harris'),
					'slug' => 'primary',
					'color' => '#1A56B0',
				],
				[
					'name'  => esc_html__('WordPress blue', 'harris'),
					'slug' => 'wordpress-blue',
					'color' => '#0073aa',
				],
				[
					'name'  => esc_html__('White', 'harris'),
					'slug' => 'white',
					'color' => '#fff',
				]
			]
		);
	}

	/**
	 * Allow the Theme to use additional core features
	 */
	public function addImageSizes()
	{
		add_image_size('post_single', 1024, 9999);
		add_image_size('gallery_mediumlarge', 1024, 768);
		add_image_size('single_xlarge', 1440, 960);
		add_image_size('single_xxlarge', 1600, 1067);
		add_image_size('single_xxxlarge', 2400, 1600);
		add_image_size('gutenberg_wide', 1280, 9999);
		add_image_size('gutenberg_full', 2560, 9999);
	}

	public function selectableImageSizes($sizes)
	{
		$sizes['gutenberg_wide'] = __('Gutenberg wide', 'harris');
		$sizes['gutenberg_full'] = __('Gutenberg full', 'harris');
		return $sizes;
	}

	/**
	 * Add navigations
	 */
	public function addNavigations()
	{
		register_nav_menus(
			[
				'primary_navigation'   => __('Primary navigation', 'harris'),
				'secondary_navigation' => __('Secondary navigation', 'harris'),
				'tertiary_navigation'  => __('Tertiary navigation', 'harris'),
			]
		);
	}

	/**
	 * Load the translation files which are delivered with the Theme
	 * Other files - stored in wp-content/languages - are loaded automatically.
	 * @return void
	 */
	public function loadTranslations()
	{
		load_theme_textdomain('harris', get_template_directory() . '/language');
	}

	/**
	 * Set the content width based on the theme's design and stylesheet
	 */
	public function contentWidth()
	{
		$GLOBALS['content_width'] = apply_filters('harris/content_width', 1024);
	}

	/**
	 * Registers styles / css files for the frontend.
	 * Assets are registered with the current theme version number.
	 *
	 * @return void
	 */
	public function registerStyles()
	{
		// wp_enqueue_style('adobe-fonts', 'https://use.typekit.net/ifk0xgd.css', [], $this->theme->Version);
		wp_enqueue_style('theme-main', get_template_directory_uri() . '/assets/dist/styles/theme.min.css', [], $this->theme->Version);
	}

	/**
	 * Load a special CSS file into backend WYSIWYG fields
	 * @return void
	 */
	public function editorStyles()
	{
		if (file_exists(get_template_directory() . '/assets/dist/styles/editor.min.css')) {
			add_editor_style(get_template_directory_uri() . '/assets/dist/styles/editor.min.css?v=' . $this->theme->Version);
		}
	}

	/**
	 * Registers scripts / js files for the frontend.
	 * Assets are registered with the current theme version number.
	 *
	 * @return void
	 */
	public function registerScripts()
	{

		if (file_exists(get_template_directory() . '/assets/dist/scripts/main.min.js')) {
			wp_enqueue_script('main', get_template_directory_uri() . '/assets/dist/scripts/main.min.js', [ 'jquery' ], $this->theme->Version, true);
		}

		$api_key = get_field('google_maps_api_key', 'theme_options');
		$location = get_post_meta(get_the_ID(), 'location', true);

		if (is_array($location) && isset($location['lat']) && isset($location['lng']) && !empty($api_key)) {
			wp_enqueue_script('googlemaps', 'https://maps.googleapis.com/maps/api/js?key='.$api_key, null, null);
		}
	}

	/**
	 * Add custom CSS classes to the post classes array
	 * @param  array $classes The pre-existing classes
	 * @return array          The potentially modified array of classes
	 */
	public function postClasses($classes)
	{
		if (has_post_thumbnail()) {
			$img = new TimberImage(get_post_thumbnail_id(get_the_ID()));
			if ($img && $img->aspect) {
				if ($img->aspect < 1) {
					$classes[] = 'with--thumbnail-tall';
				} elseif ($img->aspect == 1) {
					$classes[] = 'with--thumbnail-square';
				} elseif ($img->aspect > 2) {
					$classes[] = 'with--thumbnail-wide';
				} else {
					$classes[] = 'with--thumbnail-horizontal';
				}
			}
		}
		return $classes;
	}

	public function customizeBodyClass($classes)
	{
		$style = get_field('toolbar_style', get_the_ID());

		$classes[] = $style;

		if (get_post_meta(get_the_ID(), 'gutenberg_full', true)) {
			$classes[] = 'c-gutenbergpost';
		}

		return $classes;
	}

	public function customToolbarTextColour()
	{
		if (get_field('toolbar_style', get_the_ID()) == 'with--toolbar-transparent-custom-colour') {
			echo '<style>' . PHP_EOL . '.with--toolbar-transparent-custom-colour .nav.toolbar {color: ' . get_field('text_colour', get_the_ID()) . ';}' . PHP_EOL . '.with--toolbar-transparent-custom-colour .menu-icon {background-color: ' . get_field('text_colour', get_the_ID()) . ';}' . PHP_EOL . '</style>';
		}
	}
}
