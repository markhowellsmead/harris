<?php

/**
 * Template Name: Page builder
 */

use Timber\Timber;
use SayHello\Theme\Packages\Components;

$context = Timber::get_context();
$context['post'] = Timber::get_post();

$context['post']->video_ref = get_field('video_ref', $context['post']->ID);
$context['post']->builder = get_field('builder', $context['post']->ID);
if (!empty($context['post']->builder)) {
	foreach ($context['post']->builder as &$component) {
		$component = Components::extendComponent($component);
	}
}

Timber::render('builder.twig', $context);
