# WordPress Foundation Theme

## Konfiguration für Advanced Custom Fields

Dieses Verzeichnis beinhaltet Konfigurationsdateien für das Plugin «Advanced Custom Fields». Falls die Konfiguration 
manuell nachbearbeitet wird, oder falls die Feldergruppen in WordPress-Admin nicht sichtbar sind, musst du eventuell 
die [Synchronisierungsfunktion](https://www.advancedcustomfields.com/resources/synchronized-json/) anwenden.
