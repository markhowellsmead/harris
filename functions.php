<?php

/**
 * Dumps a variable.
 *
 * @param mixed $var The variable to be dumped.
 * @param boolean $exit Whether to exit the script after dumping.
 * @return void
 */
if (! function_exists('dump')) {
	function dump($var, $exit = false)
	{
		echo '<pre>' . print_r($var, true) . '</pre>';
		if ($exit) {
			exit;
		}
	}
}

require_once 'vendor/autoload.php';

/**
	 * Returns the Theme Instance
	 *
	 * @return Object Theme Object
	 */
if (! function_exists('sht_theme')) {
	function sht_theme()
	{
		return SayHello\Theme\Theme::getInstance();
	}
}

sht_theme();
sht_theme()->run();

$theme = new SayHello\Theme\Theme();
