<?php

use Timber\Timber;

$context = Timber::get_context();
$context['post'] = Timber::get_post();

$context['post']->hide_title = (bool)get_post_meta($context['post']->ID, 'hide_title', true);
$context['post']->video_ref = get_post_meta($context['post']->ID, 'video_ref', true);
$context['post']->images = get_field('images', $context['post']->ID);

Timber::render('page.twig', $context);
