<?php

namespace SayHello\Theme;

use Timber\Timber;

if (post_password_required()) {
	return;
}

$context = Timber::get_context();
$context['post'] = Timber::get_post();

Timber::render([
	'partials/comments-' .get_post_type(). '.twig',
	'partials/comments.twig'
], $context, false); // Param 3: do not use Timber cache!
