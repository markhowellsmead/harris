<?php

use Timber\Timber;

$context         = Timber::get_context();
$context['post'] = Timber::get_post();

$context['post']->video_ref = get_post_meta($context['post']->ID, 'video_ref', true);
$context['post']->hide_thumbnail = get_post_meta($context['post']->ID, 'hide_thumbnail', true);
$context['post']->gutenberg_full = get_post_meta($context['post']->ID, 'gutenberg_full', true);

Timber::render(
	[
		$context['post']->gutenberg_full ? 'single-' . $context['post']->type . '-gutenberg.twig' : null,
		'single-' . $context['post']->type . '.twig',
		$context['post']->format ? 'single-' . $context['post']->format . '.twig' : null,
		$context['post']->gutenberg_full ? 'single-' . $context['post']->type . '-gutenberg.twig' : null,
		$context['post']->gutenberg_full && $context['post']->format ? 'single-' . $context['post']->format . '-gutenberg.twig' : null,
		$context['post']->gutenberg_full ? 'single-gutenberg.twig' : null,
		'single.twig'
	],
	$context
);
