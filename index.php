<?php

use Timber\Timber;
use Timber\PostQuery as PostQuery;

$context = Timber::get_context();
$context['posts'] = new PostQuery();

Timber::render('index.twig', $context);
