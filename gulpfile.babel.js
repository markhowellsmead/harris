import gulp from 'gulp';
import livereload from 'gulp-livereload';

const config = {
	name: 'Harris',
	key: 'harris',
	assetsDir: 'assets/',
	gulpDir: './assets/gulp/',
	assetsDist: 'assets/dist/',
	assetsSrc: 'assets/src/',
	errorLog: function(error) {
		console.log('\x1b[31m%s\x1b[0m', error);
		if (this.emit) {
			this.emit('end');
		}
	},
	reload: [
		'*.php',
		'{Classes,inc,partials,templates,includes}/**/*.{php,html,twig}'
	]
};

import { task as taskStyles } from './assets/gulp/task-styles';
// import { task as taskScripts } from './assets/gulp/task-scripts';
// import { task as taskReload } from './assets/gulp/task-reload';
// import { task as taskSvg } from './assets/gulp/task-svg';
// import { task as taskModernizr } from './assets/gulp/task-modernizr';
// import { task as taskPot } from './assets/gulp/task-pot';
// import { task as taskServe } from './assets/gulp/task-serve';
// import { task as taskGutenberg } from './assets/gulp/task-gutenberg';

export const styles = () => taskStyles(config);
// export const scripts = () => taskScripts(config);
// export const reload = () => taskReload(config);
// export const svg = () => taskSvg(config);
// export const modernizr = () => taskModernizr(config);
// export const pot = () => taskPot(config);
// export const gutenberg = () => taskGutenberg(config);
// export const serve = () => taskServe(config);
export const watch = () => {

	const settings = { usePolling: true, interval: 100 };

	livereload.listen();

	gulp.watch(config.assetsSrc + 'styles/**/*.scss', settings, gulp.series(styles));
	// gulp.watch(config.assetsSrc + 'scripts/**/*.js', settings, gulp.series(scripts));
	// gulp.watch(config.assetsSrc + 'gutenberg/**/*.{scss,js,jsx}', settings, gulp.series(gutenberg));
	// gulp.watch([config.assetsSrc + '**/*.svg', '!' + config.assetsDir + '**/*.min.svg'], settings, gulp.series(svg));
	// gulp.watch(config.reload).on('change', livereload.changed);
};


// export const taskDefault = gulp.series(gulp.parallel(styles, scripts, reload, svg), watch);
export const taskDefault = gulp.series(gulp.parallel(styles), watch);
export default taskDefault;
