(function($) {
	// Track window scroll position and apply CSS class to HTML element
	var trackScrollPosition = function() {
		if ($(window).scrollTop() > 0) {
			$('html').addClass('with--scrolloffset');
		} else {
			$('html').removeClass('with--scrolloffset');
		}
	};

	$(document).on('ready.scrollpos', trackScrollPosition);
	$(window).on('load.scrollpos scroll.scrollpos', trackScrollPosition);
})(jQuery);
