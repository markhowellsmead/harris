require('@sayhellogmbh/js-hashscroll');

(function($) {

	$(window).on('load.anchoranimate resize.anchoranimate', function() {
		$('#wpadminbar').each(function() {
			if ($(this).outerHeight() && $(this).css('position') === 'fixed') {
				window.anchorAnimateOffset += $(this).outerHeight();
			}
		});
		window.anchorAnimateOffset = Math.floor(window.anchorAnimateOffset) - 1;
	});

	$('a[href*="#"]').anchorAnimate();

})(jQuery);
