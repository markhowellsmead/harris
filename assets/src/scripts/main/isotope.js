// jump through hoops for Isotope
var jQueryBridget = require('jquery-bridget');
require('isotope-layout');

(function($) {
	$(document).on('ready.isotope', function() {
		var $grid = $('[data-isotope-grid]').isotope({
			itemSelector: '[data-isotope-grid-item]',
			percentPosition: true,
			masonry: {
				columnWidth: '[data-isotope-grid-item]'
			}
		});

		$grid.imagesLoaded().progress(function() {
			$grid.isotope('layout');
		});

	});
})(jQuery);
