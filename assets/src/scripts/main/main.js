var $ = require('jquery');

require('./404');
require('./fancybox');
require('./hashscroll');
require('./hero');
require('./isotope');
require('./maps');
require('./scrollposition');

Swiper = require('swiper/dist/js/swiper.js');

require('imagesloaded');
require('lazysizes');
require('picturefill');

(function($) {

	'use strict';

	$(document).ready(function() {
		$('[data-open-navigation]').on('click.open-navigation', function(event) {
			event.preventDefault();
			$('html').toggleClass('c-navigation-open');
			$(this).attr('aria-expanded', $('html').hasClass('c-navigation-open') ? 'true' : 'false');
		});
	});

	$(document).on('ready.broken_links', function() {

		// Replace broken links with their content. (So that the link isn't displayed to the visitor)
		$('.broken_link').each(function() {
			$(this).replaceWith($(this).html());
		});
	});

})(jQuery);
