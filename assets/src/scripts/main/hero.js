(function($) {

	$(document).ready(function() {
		$('.c-component-hero [data-play-video]').click(function() {
			var videoID = $(this).data('play-video');
			var $player = $('#' + videoID);
			var $component = $(this).closest('.c-component-hero');
			if ($player.hasClass('is--playing')) {
				$player.removeClass('is--playing');
				$component.removeClass('is--playing');
				$player.get(0).pause();
				$(this).text($(this).data('text-paused'));
			} else {
				$player.addClass('is--playing');
				$component.addClass('is--playing');
				$player.get(0).play();
				$(this).text($(this).data('text-playing'));
			}
		});

	});

})(jQuery);
