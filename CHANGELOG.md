# harris

## Changelog

### 1.18.4 2019-09-09

- Fix search box layout

### 1.18.3 2019-09-06
### 1.18.2 2019-09-06

- Fun with CSS
- More typography and layout stuff
- Fix albums
- Add Inter font
- Comment out Adobe Fonts

### 1.18.1 2019-09-06
### 1.18.0 2019-09-06

- Postlist layout fixes
- Stretch out some header media to 1056px
- Gutenberg CSS revisions

### 1.17.6 2019-09-03
### 1.17.5 2019-09-03

- Add gutenberg_wide image size

### 1.17.4 2019-08-27

- Fix position relative on image blocks

### 1.17.3 2019-08-26

- Fix cover block colours and alignment

### 1.17.2 2019-08-26

- Fix cover block width

### 1.17.1 2019-08-26

- Change fancybox overlay colour

### 1.17.0 2019-08-26

- Better Gutenberg full layout
- Gallery block

### 1.16.6 2019-08-22

- Deregistering dashicons also removes the WP embed script. Reinstate it.

### 1.16.5 2019-08-20

- Implement Gutenberg color selections

### 1.16.4 2019-08-20

- Toolbar spacing in login status
- Typo and spacing fixes
- Add WordPress logo to appropriate postlist entries
- Remove unused fonts

### 1.16.3 2019-08-19

- Hello Cookie note color
- Fix pre/code styling
- Don’t remove core block CSS

### 1.16.2 2019-08-15

- Catch parse_url if input isn't a string

### 1.16.1 2019-08-13

- Typog improvements
- Improve primary media
- Limit post list preview text width

### 1.16.0 2019-08-13

- Switch all blog views to 500px layout

### 1.15.0 2019-07-15

- Add custom post format slugs

### 1.14.3 2019-07-14
### 1.14.2 2019-07-14
### 1.14.1 2019-07-14

- Improve primary image and video sizes
- Vertical image layout
- Add photo date
- Don’t double-wrap video HTML
- Single view improvements

### 1.14.0 2019-07-11

- Migrating photos to post format
- Improved post format output
- Various taxonomy and single output fixes

### 1.13.1 2019-06-26

- Fix primary image proportions on blog posts
- Wider 16:9 images
- Avoid PHP errors for undefined image sizes
- Fix single media output

### 1.13.0 2019-06-26

- Add Chatra app

### 1.12.4 2019-06-25

- Fix primary media output
- Fix gulpfile
- Re-locate src JavaScript files

### 1.12.3 2019-06-15
### 1.12.2 2019-06-15
### 1.12.1 2019-06-15
### 1.12.0 2019-06-15

- Gutenberg elements and wider post layout.

### 1.11.7 2019-05-19

- Begin work on HTML simplification for Gutenberg

### 1.11.6 2019-05-18

- Better post thumbnail output

### 1.11.5 2019-05-17
### 1.11.4 2019-05-17

- Add “hide thumbnail” option for blog posts

### 1.11.3 2019-05-14

- Fixes for Gutenberg CSS in WordPress 5.2

### 1.11.2 2019-05-13

- Fix `Timber\PostQuery` namespacing

### 1.11.1 2019-05-12
### 1.11.0 2019-05-12

- Add Gutenberg support and base styling :tada:
- Add latest photos to front page
- Remove Foundation JavaScript
- Split out JS into separate files
- Remove assets/class names from The Other Place
- Wider default indent size (editorconfig)
- Fix icomoon font and footer typography
- Allow page header to be hidden

### 1.10.1 2019-05-03

- PHP code tidying
- Update ruleset XML

### 1.10.0 2019-04-19

- Update provider namespaces
- Typog. tweaks
- Customize recent posts Widget
- Sidebars and widgets
- Add Sidebar to blog list view
- Base font to 16px
- Add CSS grid and post map to Photo single

### 1.9.0 2019-04-09

- Add map to single post view
- Use CSS Grid to layout post meta

### 1.8.0 2019-03-09

- Posts by tag (builder)
- Improve Hero

### 1.7.3 2019-03-05

- Fix archive headers

### 1.7.2 2019-03-05

### 1.7.1 2019-03-05

### 1.7.0 2019-03-05

- Add post list component
- Improve collections component

### 1.6.1 2019-03-01

### 1.6.0 2019-03-01

- Fix comment date
- Colour and toolbar spacing
- Photo page layout
- Dagny font and page layouts

### 1.5.1 2019-02-26

### 1.5.0 2019-02-26

- Add Say Hello Sponsor Widget

### 1.4.1 2019-02-25

- Improvements to toolbar/content rel. and spacing

### 1.4.0 2019-02-15

- Better photo posts grid
- Single-column blog list

### 1.3.0 2019-02-09

- New Icomoon font
- Add sticky post card styling to list view

### 1.2.0 2019-02-09

- Fonts (Inter) and typography
- Layout and typog. improvements
- Fix hero image

### 1.1.0 2019-01-27

- Add collections builder element

### 1.0.1 2019-01-25

- Fix search results list

### 1.0.0 2018-12-02

- Re-introduce commenting :tada: :100:
- Release 1.0.0 because this really isn't in Beta any more.

### 0.19.0 2018-11-23

- Add dark mode styling (without activation feature)

### 0.18.1 2018-11-21

- Fix tall thumbnails in post list grid

### 0.18.0 2018-11-16

- Allow Photo Posts to use video_ref as oembed field
- Save all ACF groups to JSON

### 0.17.12 2018-11-15

### 0.17.11 2018-11-15

- Add WordPress icon to icomoon set
- Adjust photo card hover/focus

### 0.17.10 2018-11-14

- Locally-hosted fonts
- Better mobile nav

### 0.17.9 2018-11-14

- Add green hover colour to photo cards
- Reverse footer icon colours

### 0.17.8 2018-11-13

- Add LinkedIn icon to icon font and drop IE8 (EOT) font support

### 0.17.7 2018-11-13

- Green footer with Icomoon icons

### 0.17.6 2018-11-13

- Version bump to clear asset caches

### 0.17.5 2018-11-13

- Navigation styling improvements

### 0.17.4 2018-11-11

- Fix gallery layouts for images with no large size

### 0.17.3 2018-11-10

- Fix flex galleries where no large version of the image is available

### 0.17.2 2018-11-08

### 0.17.1 2018-11-08

- Fix WordPress thumbnail image hover effect

### 0.17.0 2018-11-08

- Improved photo list view: lazyload and responsive images

### 0.16.5 2018-11-07

- Stop post card thumbnail area from collapsing pre-load

### 0.16.4 2018-11-05

- Force thumbnails to 100% in mobile
- Fix card padding
- Add browser sync

### 0.16.3 2018-11-03

- Stylize single photo page

### 0.16.2 2018-11-03

- Re-add missing no-posts file

### 0.16.1 2018-11-03

- Add post thumbnails support for photos

### 0.16.0 2018-11-03

- Improved photo list view

### 0.15.0 2018-10-30

- Improved photo single view

### 0.14.9 2018-10-21

- Hide gallery500 hover text

### 0.14.8 2018-10-21

- Fix ugly preload effect on gallery500

### 0.14.7 2018-10-21

- Remove post thumbnail from gallery page

### 0.14.6 2018-09-28

- Fix gallery
- Fix isotope
- Move all JS into main (delete vendor)

### 0.14.5 2018-09-27

- Move all JS into main (delete vendor)

### 0.14.4 2018-09-26

- Fix Wordpress archive view CSS

### 0.14.3 2018-09-25

- Fix overlapping header in lower res.

### 0.14.2 2018-09-25

- Improve PRE and CODE styling

### 0.14.1 2018-09-21

- Quickfix JS slider

### 0.14.0 2018-09-21

- Add flex gallery and fancybox

### 0.13.1 2018-09-19

- Improve post list in iPad

### 0.13.0 2018-09-19

- Add isotope layout for blog list
- Add custom styling for WordPress topic archive

### 0.12.3 2018-09-15

- Add editor CSS

### 0.12.2 2018-09-15

- Improved hero video play/layout

### 0.12.1 2018-09-14

- Remove image carousel autoplay

### 0.12.0 2018-09-14

- Add Hero with video and buttons

### 0.11.1 2018-09-13

- Fix RWD image sizes attributes

### 0.11.0 2018-09-13

- Primary image and video sizing
- Nicer toolbar
- JS to swap out broken links

### 0.10.4 2018-09-04

- Add fixed image sizes for single image view

### 0.10.2, 0.10.3 2018-09-04

- Improve image sizes on image post format single view

### 0.10.1 2018-09-04

- Improved flex gallery layout, sizes and image margins

### 0.10.0 2018-08-30

- Image carousel

### 0.9.3 2018-08-28

- Fix empty target attributes

### 0.9.2 2018-08-28

- Add toolbar customization options

### 0.9.1 2018-08-28

- Fix post primary image max width
- Remove DIST from repo

### 0.9.0 2018-08-28

- Simple hero
- RWD for primary image
- Improve size and quality of post primary image

### 0.8.5 2018-08-23

- Get higher-resolution YouTube thubmnail images.

### 0.8.4 2018-08-23

- Page title and mobile navi improvements

### 0.8.3 2018-08-22

### 0.8.2 2018-08-22

### 0.8.1 2018-08-22

- Reinstate missing video wrapper and parameter cleaner

### 0.8.0 2018-08-22

- Switch to Rubik font
- Detail adjustments to typography
- Standardize spacing (instead of responsive)
- Add `$content-width` value for oEmbeds.
- Larger primary video in single view

### 0.7.3 2018-08-22

- Add video thumbnails to card list
- More styling for builder

### 0.7.2 2018-08-21

- Remove top margin from first chld caption elements

### 0.7.1 2018-08-21

- Adjust header sizes

### 0.7.0 2018-08-21

- Add page builder

### 0.6.0 2018-08-21

- Reintroduce gallery post galleries

### 0.5.0 2018-08-18

- Image above title on single image posts

### 0.4.0 2018-08-16

- Theme options ACF group
- Minor spacing adjustments
- Add theme options page and Maps API key
- Allow page thumbnails
- Move header and primary media out to components

### 0.3.1 2018-08-16

- Remove sticky function from toolbar and make it fixed for medium up.

### 0.3.0 2018-08-16

- Sticky toolbar and styling
- Add SVG support for media uploads

### 0.2.9 2018-08-16

- Adjust card margins and shadows and footer spacing/gradient

### 0.2.8 2018-08-15

- Catch replicated ACF data

### 0.2.7 2018-08-15

- Better post list image scaling

### 0.2.6 2018-08-15

- Better post list image gradients

### 0.2.5 2018-08-15

### 0.2.4 2018-08-15

- Add post formats support

### 0.2.3 2018-08-15

- Improve card list
- Improve vertical post thumbnail output

### 0.2.2 2018-08-14

- Add dist folder to repo

### 0.2.1 2018-08-14

### 0.2.0 2018-08-14

- Project-specific styling and HTML.

### 0.1.0 2018-08-10

- First port from wptheme.blog.

### 0.0.0 2018-00-00

- Feature: This is an example comment. Types can be Feature, Bugfix or General.
