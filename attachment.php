<?php

/**
 * This template will be used when an Attachment post is called in the frontend
 * mhm 7.11.2017
 */

wp_redirect(wp_get_attachment_url(), 301);
