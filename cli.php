<?php

require('../../../wp-blog-header.php');
require_once("../../../wp-config.php");
require_once("../../../wp-includes/wp-db.php");

$posts_processed = 0;

$query = new WP_Query([
	'post_type' => 'post',
	'cache_results' => false,
	'posts_per_page' => -1,
	'tax_query' => [
		'relation' => 'AND',
			// [
			// 	'taxonomy' => 'post_format',
			// 	'field' => 'slug',
			// 	'terms' => ['post-format-image'],
			// 	'operator' => 'NOT IN'
			// ],
			// [
			// 	'taxonomy' => 'collection',
			// 	'field' => 'slug',
			// 	'terms' => ['flickr-archives'],
			// 	'operator' => 'IN'
			// ],
			[
				'taxonomy' => 'post_format',
				'field' => 'slug',
				'terms' => ['image'],
				'operator' => 'IN'
			],
	]
]);

while ($query->have_posts()) {
	global $post;
	$query->the_post();
	$posts_processed++;
	set_post_format($post, 'image');
}

wp_reset_postdata();
var_dump($posts_processed . ' posts processed. ');
